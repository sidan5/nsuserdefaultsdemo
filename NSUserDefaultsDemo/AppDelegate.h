//
//  AppDelegate.h
//  NSUserDefaultsDemo
//
//  Created by Idan on 08/04/2016.
//  Copyright © 2016 TestOrg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

