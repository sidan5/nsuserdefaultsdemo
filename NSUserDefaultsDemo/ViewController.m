//
//  ViewController.m
//  NSUserDefaultsDemo
//
//  Created by Idan on 08/04/2016.
//  Copyright © 2016 TestOrg. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"[[NSUserDefaults standardUserDefaults] integerForKey:First_RUN] = %ld", (long)[[NSUserDefaults standardUserDefaults] integerForKey:@"First_RUN"]);
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"First_RUN"] == NULL) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"First_RUN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    NSLog(@"[[NSUserDefaults standardUserDefaults] integerForKey:First_RUN = %ld", (long)[[NSUserDefaults standardUserDefaults] integerForKey:@"First_RUN"]);
}

@end
